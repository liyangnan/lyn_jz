package com.briup.jz.web.controller;

import com.briup.jz.bean.AccountSystem;
import com.briup.jz.service.IAccountSystemService;
import com.briup.jz.utils.Message;
import com.briup.jz.utils.MessageUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

@Api(description = "系统账号管理")
@RestController
@RequestMapping("AccountSystem")
public class AccountSystemController {
@Autowired
    private IAccountSystemService accountSystemService;
@GetMapping("findAll")
@ApiOperation(value = "查看全部信息")
    public Message findAll(){
       List<AccountSystem> list = accountSystemService.findAll();
       return MessageUtil.success(list);

}
        @PostMapping("saveOrUpdate")
        @ApiOperation(value = "保存或更新信息")
    public Message saveOrUpdate(AccountSystem accountSystem){
            accountSystemService.saveOrUpdate(accountSystem);
            return MessageUtil.success("保存成功");
        }
        @GetMapping("selectById")
    @ApiOperation(value="根据id查询")
    public Message selectById(long id){
   List<AccountSystem> List = accountSystemService.selectById(id);
   return MessageUtil.success(List);
        }
       @GetMapping("selectByUserid")
       @ApiOperation(value="根据用户id查询")
       @ApiImplicitParam(name = "userId", value = "用户唯一编号", required = true, paramType = "query")
public Message selectByUserid(long userid){
           List<AccountSystem> List = accountSystemService.selectByuserId(userid);
           return MessageUtil.success(List);
}
}


