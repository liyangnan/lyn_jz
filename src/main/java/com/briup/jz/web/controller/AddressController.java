package com.briup.jz.web.controller;

import com.briup.jz.bean.Address;
import com.briup.jz.bean.extend.AddressExtend;
import com.briup.jz.service.IAddressService;
import com.briup.jz.utils.Message;
import com.briup.jz.utils.MessageUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @description: 地址管理控制器
 * @Author: lyn
 * @Date 2020/6/14 11:19
 */
@Api(description = "地址管理接口")
@RestController
@RequestMapping("/address")
public class AddressController {

    @Autowired
    private IAddressService addressService;

    @ApiOperation(value = "查询所有地址信息")
    @GetMapping("findAll")
    public Message findAll(){
        List<Address> list = addressService.findAll();
        return MessageUtil.success(list);
    }

    @ApiOperation(value = "添加或修改地址信息")
    @PostMapping("saveOrUpdate")
    public Message saveOrUpdate(Address address){
        addressService.saveOrUpdate(address);
        return MessageUtil.success("添加或修改成功");
    }

    @ApiOperation(value = "根据id删除地址信息")
    @GetMapping("deleteById")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id",value = "唯一编号",required = true,paramType = "query")
    })
    public Message deleteById(long id){
        addressService.deleteById(id);
        return MessageUtil.success("删除成功");
    }

    @ApiOperation(value = "级联查询用户")
    @GetMapping("findAllWithUser")
    public Message findAllWithUser(){
        List<AddressExtend> list = addressService.findAllWithUser();
        return MessageUtil.success(list);
    }

}
