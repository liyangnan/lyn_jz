package com.briup.jz.web.controller;

import com.briup.jz.bean.AccountCustomer;
import com.briup.jz.service.IAccountCustomerService;
import com.briup.jz.utils.Message;
import com.briup.jz.utils.MessageUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
@Api(description = "顾客账号管理")
@RestController
@RequestMapping("AccountCustomer")
public class AccountCustomerController {
    @Autowired
    private IAccountCustomerService accountCustomerService;
    @ApiOperation(value = "查看全部信息")
    @GetMapping("findAll")
    public Message findAll(){
        List<AccountCustomer> list = accountCustomerService.findAll();
        return MessageUtil.success(list);
    }

    @PostMapping("saveOrUpdate")
    public Message saveOrUpdate(AccountCustomer accountCustomer){
        accountCustomerService.saveOrUpdate(accountCustomer);
        return MessageUtil.success("保存成功");
    }

    @GetMapping("selectById")
    @ApiOperation(value = "根据id查询")
    public Message selectById(long id){
       List<AccountCustomer> List = accountCustomerService.selectById(id);
        return MessageUtil.success(List);
    }

    @ApiOperation(value = "根据用户id查询")
    @ApiImplicitParam(name = "userId", value = "用户唯一编号", required = true, paramType = "query")
    @GetMapping("selectByuserId")
    public Message selectAllByUserId(long userId){
        List<AccountCustomer> list = accountCustomerService.selectByuserId(userId);
        return MessageUtil.success(list);
    }
}

