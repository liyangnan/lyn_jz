package com.briup.jz.web.controller;

import com.briup.jz.bean.Privilege;
import com.briup.jz.bean.extend.PrivilegeExtend;
import com.briup.jz.service.IPrivilegeService;
import com.briup.jz.utils.Message;
import com.briup.jz.utils.MessageUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


@RestController
@RequestMapping("/privilege")
@Api(description = "权限管理接口")
public class PrivilegeController {
    @Autowired
    private IPrivilegeService privilegeService;

    @ApiOperation(value = "查询全部权限信息")
    @GetMapping("findAll")
    public Message findAll(){
        List<Privilege> list =  privilegeService.findAll();
        return MessageUtil.success(list);
    }

    @ApiOperation(value = "保存或更新权限信息")
    @PostMapping("saveOrUpdate")
    public Message saveOrUpdate(Privilege privilege){
        privilegeService.saveOrUpdate(privilege);
        return MessageUtil.success("保存或更新成功");
    }

    @ApiOperation(value = "通过id删除权限信息")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id" ,value = "唯一编号", required = true, paramType = "query")
    })
    @GetMapping("deleteById")
    public Message deleteById(long id){
        privilegeService.deleteById(id);
        return MessageUtil.success("删除成功");
    }

    @ApiOperation(value = "查询所有权限，并且级联获得权限信息")
    @GetMapping("findAllWithChild")
    public List<PrivilegeExtend> findAllWithChild(){
        return privilegeService.findAllWithChild();
    }

}