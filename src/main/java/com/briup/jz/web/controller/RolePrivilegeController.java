package com.briup.jz.web.controller;

import com.briup.jz.bean.RolePrivilege;
import com.briup.jz.bean.extend.RolePrivilegeExtend;
import com.briup.jz.service.IRolePrivilegeService;
import com.briup.jz.utils.Message;
import com.briup.jz.utils.MessageUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;



@RestController
@RequestMapping("/rolePrivilege")
@Api(description = "角色权限管理接口")
public class RolePrivilegeController { @Autowired
private IRolePrivilegeService rolePrivilegeService;

    @ApiOperation(value = "查询所有角色权限")
    @GetMapping("findAll")
    public Message findAll(){
        List<RolePrivilege> list =  rolePrivilegeService.findAll();
        return MessageUtil.success(list);
    }
    @ApiOperation(value = "查询所有角色,并级联其拥有的权限")
    @GetMapping("findAllWithRole")
    public Message findAllWithRole(){

        List<RolePrivilegeExtend> list = rolePrivilegeService.findAllWithRole();
        return MessageUtil.success(list);
    }
    @ApiOperation(value = "查询所有权限，并级联可以使用的角色")
    @GetMapping("findAllWithPrivilege")
    public Message findAllWithPrivilege(){

        List<RolePrivilegeExtend> list = rolePrivilegeService.findAllWithPrivilege();
        return MessageUtil.success(list);
    }
}
