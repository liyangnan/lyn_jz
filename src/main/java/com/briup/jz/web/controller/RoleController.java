package com.briup.jz.web.controller;

import com.briup.jz.bean.Role;
import com.briup.jz.bean.extend.RoleExtend;
import com.briup.jz.service.IRoleService;
import com.briup.jz.utils.Message;
import com.briup.jz.utils.MessageUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


@RestController
@RequestMapping("/role")
@Api(description = "角色管理接口")
public class RoleController {
    @Autowired
    private IRoleService roleService;

    @ApiOperation(value = "查询所有角色信息")
    @GetMapping("findAll")
    public Message findAll(){
        List<Role> list =  roleService.findAll();
        return MessageUtil.success(list);
    }

    @ApiOperation(value = "查询角色")
    @GetMapping("findAllRole")
    public Message findAllRole(){
        List<RoleExtend> list = roleService.findAllRole();
        return MessageUtil.success(list);
    }

    @ApiOperation(value = "保存或更新角色信息")
    @PostMapping("saveOrUpdate")
    public Message saveOrUpdate(Role role){
        roleService.saveOrUpdate(role);
        return MessageUtil.success("保存或更新成功");
    }

    @ApiOperation(value = "通过id删除角色信息")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id" ,value = "唯一编号", required = true, paramType = "query")
    })
    @GetMapping("deleteById")
    public Message deleteById(long id){
        roleService.deleteById(id);
        return MessageUtil.success("删除失败");
    }
}
