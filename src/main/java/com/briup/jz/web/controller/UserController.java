package com.briup.jz.web.controller;

import com.briup.jz.bean.User;
import com.briup.jz.bean.extend.UserExtend;
import com.briup.jz.service.IUserService;
import com.briup.jz.utils.Message;
import com.briup.jz.utils.MessageUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Api(description = "用户管理接口")
@RestController
@RequestMapping("/user")
public class UserController {
    @Autowired
    private IUserService userService;

    @ApiOperation(value = "查询所有用户信息")
    @GetMapping("findAll")
    public Message findAll(){
        List<User> list = userService.findAll();
        return MessageUtil.success(list);
    }

    @ApiOperation(value = "查询所有员工")
    @GetMapping("findAllEmployee")
    public Message findAllEmployee(){
        List<UserExtend> list = userService.findAllEmployee();
        return MessageUtil.success(list);
    }

    @ApiOperation(value = "查询所有顾客")
    @GetMapping("findAllCustomer")
    public Message findAllCustomer(){
        List<UserExtend> list = userService.findAllCustomer();
        return MessageUtil.success(list);
    }

    @ApiOperation(value = "审核通过")
    @GetMapping("auditing")
    public Message auditing(long id){
        User user = userService.findById(id);
        user.setStatus("启用");
        userService.saveOrUpdate(user);
        return MessageUtil.success("审核通过");
    }

    @ApiOperation(value = "审核不通过")
    @GetMapping("refuseauditing")
    public Message refuseauditing(long id){
        User user = userService.findById(id);
        user.setStatus("禁用");
        userService.saveOrUpdate(user);
        return MessageUtil.success("已拒绝审核");
    }

    @ApiOperation(value = "添加或修改用户信息")
    @PostMapping("saveOrUpdate")
    public Message saveOrUpdate(User user){
        userService.saveOrUpdate(user);
        return MessageUtil.success("添加或修改成功");
    }

    @ApiOperation(value = "通过id删除用户信息")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id",value = "唯一编号",required = true,paramType = "query")
    })
    @GetMapping("deleteById")
    public Message deleteById(long id){
        userService.deleteById(id);
        return MessageUtil.success("删除成功");
    }

    @ApiOperation(value = "级联查询用户角色")
    @GetMapping("findAllWithRole")
    public Message findAllWithRole(){
        List<UserExtend> list = userService.findAllWithRole();
        return MessageUtil.success(list);
    }
}
