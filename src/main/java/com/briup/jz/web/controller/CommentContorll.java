package com.briup.jz.web.controller;

import com.briup.jz.bean.Category;
import com.briup.jz.bean.Comment;
import com.briup.jz.bean.extend.CategoryExtend;
import com.briup.jz.bean.extend.CommentExtend;
import com.briup.jz.service.ICategoryService;
import com.briup.jz.service.ICommentService;
import com.briup.jz.utils.Message;
import com.briup.jz.utils.MessageUtil;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Api(description = "订单评价相关接口")
@RestController
@RequestMapping("/comment")
public class CommentContorll {

        @Autowired
        private ICommentService commentService;

        @ApiOperation(value = "查询所有评论")
        @GetMapping("findAll")
        public Message findAll(){
            List<Comment> list = commentService.findAll();
            return MessageUtil.success(list);
        }

        @ApiOperation(value = "通过id删除评论")
        @GetMapping("delectById")
        @ApiImplicitParams({
                @ApiImplicitParam(name = "id",value = "唯一编号",required = true,paramType = "query")
        })
        public Message deleteById(long id){
            commentService.deleteById(id);
            return MessageUtil.success("删除成功");
        }
        @ApiOperation(value = "添加评论")
        @GetMapping("saveOrUpdate")
        public Message saveOrUpdate(Comment comment){
            commentService.saveOrUpdate(comment);
            return MessageUtil.success("添加或修改成功");
        }

    @ApiOperation(value = "查询所有评论")
    @GetMapping("findAllComment")
    public Message findAllComment(){
        List<CommentExtend> list = commentService.findAllComment();
        return MessageUtil.success(list);
    }

    @ApiOperation(value = "审核通过")
    @GetMapping("auditing")
    public Message auditing(long id)  {
        Comment comment = commentService.findById(id);
        comment.setStatu("审核通过");
        commentService.saveOrUpdate(comment);
        return MessageUtil.success("审核通过");
    }

    @ApiOperation(value = "审核不通过")
    @GetMapping("refuseauditing")
    public Message refuseauditing(long id)  {
        Comment comment = commentService.findById(id);
        comment.setStatu("审核未通过");
        commentService.saveOrUpdate(comment);
        return MessageUtil.success("已拒绝审核");
    }

}
