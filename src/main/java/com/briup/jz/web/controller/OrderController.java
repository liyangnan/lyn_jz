package com.briup.jz.web.controller;

import com.briup.jz.bean.Order;
import com.briup.jz.bean.extend.OrderExtend;
import com.briup.jz.service.IOrderService;
import com.briup.jz.utils.Message;
import com.briup.jz.utils.MessageUtil;
import com.briup.jz.vm.OrderVM;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @description: 订单管理控制器
 * @Author: lyn
 * @Date 2020/6/12 21:47
 */
@Api(description = "订单管理接口")
@RestController
@RequestMapping("/order")
public class OrderController {

    @Autowired
    private IOrderService orderService;

    @ApiOperation(value = "提交订单")
    @PostMapping("commit")
    public Message comit(@RequestBody OrderVM orderVM){
        orderService.commit(orderVM);
        return MessageUtil.success("提交成功");
    }

    @ApiOperation(value = "根据订单状态查询订单")
    @GetMapping("query")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "status",value = "订单状态",required = false,paramType = "query")
    })
    public Message query(String status){
        List<OrderExtend> list = orderService.query(status);
        return MessageUtil.success(list);
    }

    @ApiOperation(value = "查询订单")
    @GetMapping("findAll")
    public Message findAll(){
        List<Order> list = orderService.findeAll();
        return MessageUtil.success(list);
    }

    @ApiOperation(value = "根据订单id具体查询")
    @GetMapping("findOrderDetalsById")
    public Message findOrderDetalsById(long id){
        orderService.findOrderDetalsById(id);
        return MessageUtil.success(orderService.findOrderDetalsById(id));
    }

    @ApiOperation(value = "支付订单")
    @GetMapping("payOrder")
    public Message payOrder(long orderId) throws Exception{
        orderService.payOrder(orderId);
        return MessageUtil.success("success");
    }

    @ApiOperation(value = "派出订单")
    @GetMapping("sendOrder")
    public Message sendOrder(long orderId,long employeeId) throws Exception{
        orderService.sendOrder(orderId,employeeId);
        return MessageUtil.success("success");
    }

    @ApiOperation(value = "服务订单")
    @GetMapping("rejectOrder")
    public Message rejectOrder(long orderId) throws Exception{
        orderService.rejectOrder(orderId);
        return MessageUtil.success("success");
    }

    @ApiOperation(value = "确认订单")
    @GetMapping("confirmOrder")
    public Message confirmOrder(long orderId) throws Exception{
        orderService.confirmOrder(orderId);
        return MessageUtil.success("success");
    }
}
