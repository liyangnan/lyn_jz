package com.briup.jz.web.controller;

import com.briup.jz.bean.AccountEmployee;
import com.briup.jz.service.IAccountEmployeeService;
import com.briup.jz.utils.Message;
import com.briup.jz.utils.MessageUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Api(description = "员工账号管理")
@RestController("AccountEmployee")
@RequestMapping("AccountEmplyee")
public class AccountEmployeeController {
    @Autowired
    private IAccountEmployeeService accountEmployeeService;
    @ApiOperation(value = "查看全部信息")
    @GetMapping("findAll")
    public Message findAll(){
        List<AccountEmployee> list = accountEmployeeService.findAll();
        return MessageUtil.success(list);
    }
    @PostMapping("saveOrUpdate")
    public Message saveOrUpdate(AccountEmployee accountEmployee){
        accountEmployeeService.saveOrUpdate(accountEmployee);
        return MessageUtil.success("保存成功");
    }

        @GetMapping("selectById")
        @ApiOperation(value="根据id查询")
        public Message selectById(long id){
            List<AccountEmployee> list = accountEmployeeService.selectById(id);
            return MessageUtil.success(list);
}
    @GetMapping("selectByuserId")
    @ApiOperation(value="根据用户id查询")
    @ApiImplicitParam(name = "userId", value = "用户唯一编号", required = true, paramType = "query")
    public Message selectByuserId(long userId){
        List<AccountEmployee> list = accountEmployeeService.selectByuserId(userId);
        return MessageUtil.success(list);
    }


}
