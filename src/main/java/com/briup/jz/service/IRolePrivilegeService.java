package com.briup.jz.service;

import com.briup.jz.bean.RolePrivilege;
import com.briup.jz.bean.extend.RolePrivilegeExtend;

import java.util.List;



public interface IRolePrivilegeService {
    List<RolePrivilege> findAll();

    List<RolePrivilegeExtend> findAllWithRole();

    List<RolePrivilegeExtend> findAllWithPrivilege();
}
