package com.briup.jz.service.impl;

import com.briup.jz.bean.Category;
import com.briup.jz.bean.CategoryExample;
import com.briup.jz.bean.Comment;
import com.briup.jz.bean.CommentExample;
import com.briup.jz.bean.extend.CommentExtend;
import com.briup.jz.dao.CategoryMapper;
import com.briup.jz.dao.CommentMapper;
import com.briup.jz.dao.extend.CategoryExtendMapper;
import com.briup.jz.dao.extend.CommentExtendMapper;
import com.briup.jz.service.ICommentService;
import com.briup.jz.utils.CustomerException;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class CommentServiceImpl implements ICommentService {

    @Resource
    private CommentMapper commentMapper;
    @Resource
    private CommentExtendMapper commentExtendMapper;


    @Override
    public List<Comment> findAll() {
        CommentExample example = new CommentExample();
        return commentMapper.selectByExample(example);
    }

    @Override
    public List<CommentExtend> findAllComment() {
        CommentExample example = new CommentExample();
        return commentExtendMapper.selectAllComment();
    }

    @Override
    public Comment findById(long id) {
        return commentMapper.selectByPrimaryKey(id);
    }

    @Override
    public List<CommentExtend> findAllWithPl() {
        return commentExtendMapper.selectAllWithPl();
    }

    @Override
    public void deleteById(long id) throws CustomerException{
        //        先判断该id对应的数据存在与否
        Comment comment = commentMapper.selectByPrimaryKey(id);
//        如果存在，进行正常删除，如果不存在，进行报错
        if(comment == null) {
//            不存在，报错
            throw new CustomerException("删除失败，要删除的数据不存在");
        }
//        存在，进行正常删除
        commentMapper.deleteByPrimaryKey(id);

    }


    @Override
    public void saveOrUpdate(Comment comment) {
        if(comment.getId() !=null){
            commentMapper.updateByPrimaryKey(comment);
        }else{
            commentMapper.insert(comment);
        }
    }

}
