package com.briup.jz.service.impl;

import com.briup.jz.bean.AccountEmployee;
import com.briup.jz.bean.AccountEmployeeExample;
import com.briup.jz.dao.AccountEmployeeMapper;
import com.briup.jz.service.IAccountEmployeeService;
import com.briup.jz.utils.CustomerException;
import org.springframework.boot.autoconfigure.web.ConditionalOnEnabledResourceChain;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;



@Service
public class AccountEmployeeServiceImpl implements IAccountEmployeeService {
    @Resource
    private AccountEmployeeMapper accountEmployeeMapper;
    @Override
    public List<AccountEmployee> findAll() {

        AccountEmployeeExample example = new AccountEmployeeExample();
        return accountEmployeeMapper.selectByExample(example);

    }

    @Override
    public void saveOrUpdate(AccountEmployee accountEmployee) {
        if(accountEmployee.getId() !=null){
            accountEmployeeMapper.updateByPrimaryKey(accountEmployee);
        }else {
            accountEmployeeMapper.insert(accountEmployee);
        }

    }

    @Override
    public List<AccountEmployee> selectById(long id) {

          AccountEmployeeExample example = new AccountEmployeeExample();
          AccountEmployeeExample.Criteria criteria = example.createCriteria();
          criteria.andUserIdEqualTo(id);
          return accountEmployeeMapper.selectByExample(example);

    }

    @Override
    public List<AccountEmployee> selectByuserId(long userId) throws CustomerException {
        AccountEmployeeExample example = new AccountEmployeeExample();
        AccountEmployeeExample.Criteria criteria = example.createCriteria();
        criteria.andIdEqualTo(userId);
        return accountEmployeeMapper.selectByExample(example);
    }



}
