package com.briup.jz.service.impl;

import com.briup.jz.bean.RolePrivilegeExample;
import com.briup.jz.bean.RolePrivilege;
import com.briup.jz.bean.extend.RolePrivilegeExtend;
import com.briup.jz.dao.RolePrivilegeMapper;
import com.briup.jz.dao.extend.RolePrivilegeExtendMapper;
import com.briup.jz.service.IRolePrivilegeService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;


@Service
public class RolePrivilegeServiceImpl implements IRolePrivilegeService {
    @Resource
    private RolePrivilegeMapper rolePrivilegeMapper;
    @Resource
    private RolePrivilegeExtendMapper rolePrivilegeExtendMapper;

    @Override
    public List<RolePrivilege> findAll() {
        RolePrivilegeExample example = new RolePrivilegeExample();
        return rolePrivilegeMapper.selectByExample(example);
    }

    @Override
    public List<RolePrivilegeExtend> findAllWithRole() {
        return rolePrivilegeExtendMapper.selectAllWithRole();
    }

    @Override
    public List<RolePrivilegeExtend> findAllWithPrivilege() {
        return rolePrivilegeExtendMapper.selectAllWithPrivilege();
    }
}
