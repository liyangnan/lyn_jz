package com.briup.jz.service.impl;

import com.briup.jz.bean.Category;
import com.briup.jz.bean.CategoryExample;
import com.briup.jz.bean.extend.CategoryExtend;
import com.briup.jz.dao.CategoryMapper;
import com.briup.jz.dao.extend.CategoryExtendMapper;
import com.briup.jz.service.ICategoryService;
import com.briup.jz.utils.CustomerException;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class CategoryServiceImpl implements ICategoryService {

    @Resource
    private CategoryMapper categoryMapper;
    @Resource
    private CategoryExtendMapper categoryExtendMapper;
    @Override
    public List<Category> findAll() {
        CategoryExample example = new CategoryExample();
        return categoryMapper.selectByExample(example);
    }

    @Override
    public void saveOrUpdate(Category category) throws CustomerException {
        if(category.getId() !=null){
            categoryMapper.updateByPrimaryKey(category);
        }else{
            categoryMapper.insert(category);
        }
    }

    @Override
    public void deleteById(long id) throws CustomerException{
//        先判断该id对应的数据存在与否
        Category category = categoryMapper.selectByPrimaryKey(id);
//        如果存在，进行正常删除，如果不存在，进行报错
        if(category == null) {
//            不存在，报错
            throw new CustomerException("删除失败，要删除的数据不存在");
        }
//        存在，进行正常删除
        categoryMapper.deleteByPrimaryKey(id);

    }

    @Override
    public List<CategoryExtend> findAllWithChild() {

        return categoryExtendMapper.selectAllWithChild();
    }
}
