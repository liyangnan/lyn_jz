package com.briup.jz.service.impl;

import com.briup.jz.bean.User;
import com.briup.jz.bean.UserExample;
import com.briup.jz.bean.extend.UserExtend;
import com.briup.jz.dao.UserMapper;
import com.briup.jz.dao.extend.UserExtendMapper;
import com.briup.jz.service.IUserService;
import com.briup.jz.utils.CustomerException;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
@Service
public class UserServiceImpl implements IUserService {

    @Resource
    private UserMapper userMapper;
    @Resource
    private UserExtendMapper userExtendMapper;
    @Override
    public List<User> findAll() {
        UserExample example = new UserExample();
        return userMapper.selectByExample(example);
    }

    @Override
    public List<UserExtend> findAllEmployee() {
        UserExample example = new UserExample();
        return userExtendMapper.selectAllEmployee();
    }

    @Override
    public List<UserExtend> findAllCustomer() {
        UserExample example = new UserExample();
        return userExtendMapper.selectAllCustomer();
    }

    @Override
    public void saveOrUpdate(User user) throws CustomerException {
        if(user.getId() !=null){
            userMapper.updateByPrimaryKey(user);
        }else{
            userMapper.insert(user);
        }
    }

    @Override
    public void deleteById(long id) throws CustomerException{
//        先判断该id对应的数据存在与否
        User user = userMapper.selectByPrimaryKey(id);
//        如果存在，进行正常删除，如果不存在，进行报错
        if(user == null){
            //当不存在，报错
          throw new CustomerException("删除失败，要删除的数据不存在");
        }
//        当存在，进行删除
        userMapper.deleteByPrimaryKey(id);
    }

    @Override
    public List<UserExtend> findAllWithRole() {
        return userExtendMapper.selectAllWithRole();
    }

    @Override
    public User findById(long id) {
        return userMapper.selectByPrimaryKey(id);
    }

}
