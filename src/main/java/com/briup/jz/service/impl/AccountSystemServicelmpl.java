package com.briup.jz.service.impl;

import com.briup.jz.bean.AccountSystem;
import com.briup.jz.bean.AccountSystemExample;
import com.briup.jz.dao.AccountSystemMapper;
import com.briup.jz.dao.extend.AccountSystemExtendMapper;
import com.briup.jz.service.IAccountSystemService;
import com.briup.jz.utils.CustomerException;
import org.omg.CORBA.SystemException;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;


@Service
public class AccountSystemServicelmpl implements IAccountSystemService {
    @Resource
    private AccountSystemMapper accountSystemMapper;
    @Resource
    private AccountSystemExtendMapper accountSystemExtendMapper;

    @Override
    public List<AccountSystem> findAll() {
        AccountSystemExample example = new AccountSystemExample();
        return accountSystemMapper.selectByExample(example);
    }

    @Override
    public void saveOrUpdate(AccountSystem accountSystem) throws SystemException {
        if (accountSystem.getId() != null){
            accountSystemMapper.updateByPrimaryKey(accountSystem);

        }else {
            accountSystemMapper.updateByPrimaryKey(accountSystem);
        }

    }

    @Override
    public List<AccountSystem> selectById(long id) throws CustomerException {
        AccountSystemExample example = new AccountSystemExample();
        AccountSystemExample.Criteria criteria = example.createCriteria();
        criteria.andIdEqualTo(id);
        return accountSystemMapper.selectByExample(example);
    }


    @Override
    public List<AccountSystem> selectByuserId(long userId) throws CustomerException {
        AccountSystemExample example = new AccountSystemExample();
        AccountSystemExample.Criteria criteria = example.createCriteria();
        criteria.andUserIdEqualTo(userId);
        return accountSystemMapper.selectByExample(example);
    }




}
