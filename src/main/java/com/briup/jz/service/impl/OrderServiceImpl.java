package com.briup.jz.service.impl;

import com.briup.jz.bean.Order;
import com.briup.jz.bean.OrderExample;
import com.briup.jz.bean.OrderLine;
import com.briup.jz.bean.User;
import com.briup.jz.bean.extend.OrderExtend;
import com.briup.jz.dao.OrderLineMapper;
import com.briup.jz.dao.OrderMapper;
import com.briup.jz.dao.UserMapper;
import com.briup.jz.dao.extend.OrderExtendMapper;
import com.briup.jz.service.IOrderService;
import com.briup.jz.utils.CustomerException;
import com.briup.jz.vm.OrderVM;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

/**
 * @description: 订单业务实现类
 * @Author: lyn
 * @Date 2020/6/12 22:41
 */
@Service
public class OrderServiceImpl implements IOrderService {
    @Resource
    private OrderMapper orderMapper;
    @Resource
    private UserMapper userMapper;
    @Resource
    private OrderExtendMapper orderExtendMapper;
    @Resource
    private OrderLineMapper orderLineMapper;

    @Override
    public void commit(OrderVM orderVM) throws CustomerException {
        /*
        * 1、分别获取到订单信息 及订单项信息
        * 2、先保存订单信息
        * 3、然后保存订单项信息
        * */
        Order order = new Order();
        order.setTotal(orderVM.getTotal());
        order.setAddressId(orderVM.getAddressId());
        order.setCustomerId(orderVM.getCustomerId());
        order.setEmployeeId(orderVM.getEmployeeId());
        order.setOrderTime(new Date().getTime());
        order.setStatus(OrderExtend.STATUS_WFK);

        List<OrderLine> orderLines = orderVM.getOrderLines();

        orderMapper.insert(order);

        for(OrderLine orderLine : orderLines){
//            设置order与orderline之间的关系
            orderLine.setOrderId(order.getId());
            orderLineMapper.insert(orderLine);
        }
    }

    @Override
    public List<Order> findeAll() {
        return orderMapper.selectByExample(new OrderExample());
    }

    @Override
    public List<OrderExtend> query(String status) {
        return orderExtendMapper.query(null,status);
    }

    @Override
    public OrderExtend findOrderDetalsById(long id) {
        List<OrderExtend> list = orderExtendMapper.query(id,null);
        if(list.size()>0){
            return list.get(0);
        }
        return null;
    }

    @Override
    public void payOrder(long orderId) throws CustomerException{
        Order order = orderMapper.selectByPrimaryKey(orderId);
        if(order == null){
            throw new CustomerException("该订单不存在");
        }
        order.setStatus(OrderExtend.STATUS_DPD);
        orderMapper.updateByPrimaryKey(order);
    }

    @Override
    public void sendOrder(long orderId, long employeeId) throws CustomerException{
        Order order = orderMapper.selectByPrimaryKey(orderId);
        if(order == null){
            throw new CustomerException("该订单不存在");
        }else if(!order.getStatus().equals("待派单")){
            throw new CustomerException("订单未支付/订单异常");
        }
        User emp = userMapper.selectByPrimaryKey(employeeId);
        if(emp == null){
            throw new CustomerException("该员工不存在");
        }
        order.setStatus(OrderExtend.STATUS_DFW);
        orderMapper.updateByPrimaryKey(order);
    }

    //    如果业务上派单不需要员工主动接单 而是系统直接分配 就不需要此步
//    @Override
//    public void takeOrder(long orderId) throws Exception{
//
//    }

    @Override
    public void rejectOrder(long orderId) throws CustomerException{
        Order order = orderMapper.selectByPrimaryKey(orderId);
        if(order == null){
            throw new CustomerException("该订单不存在");
        }else if(!order.getStatus().equals("待服务")){
            throw new CustomerException("订单待服务/订单异常");
        }
        order.setStatus(OrderExtend.STATUS_DQR);
        orderMapper.updateByPrimaryKey(order);
    }

    @Override
    public void confirmOrder(long orderId) throws CustomerException{
        Order order = orderMapper.selectByPrimaryKey(orderId);
        if(order == null){
            throw new CustomerException("该订单不存在");
        }else if(!order.getStatus().equals("待确认")){
            throw new CustomerException("订单待确认/订单异常");
        }
        order.setStatus(OrderExtend.STATUS_YWC);
        orderMapper.updateByPrimaryKey(order);
    }


}
