package com.briup.jz.service.impl;

import com.briup.jz.bean.Privilege;
import com.briup.jz.bean.PrivilegeExample;
import com.briup.jz.bean.extend.PrivilegeExtend;
import com.briup.jz.dao.PrivilegeMapper;
import com.briup.jz.dao.extend.PrivilegeExtendMapper;
import com.briup.jz.service.IPrivilegeService;
import com.briup.jz.utils.CustomerException;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class PrivilegeServiceImpl implements IPrivilegeService{
    @Resource
    private PrivilegeMapper privilegeMapper;
    @Resource
    private PrivilegeExtendMapper privilegeExtendMapper;

    @Override
    public List<Privilege> findAll() {
        PrivilegeExample example = new PrivilegeExample();
        return privilegeMapper.selectByExample(example);
    }

    @Override
    public void saveOrUpdate(Privilege privilege) throws CustomerException {
        if(privilege.getId() !=null){
            privilegeMapper.updateByPrimaryKey(privilege);
        } else {
            privilegeMapper.insert(privilege);
        }
    }

    @Override
    public void deleteById(long id) throws CustomerException {
        // 先判断该id对应的数据存在不？
        Privilege privilege = privilegeMapper.selectByPrimaryKey(id);
        if(privilege == null){
            //当不存在，报错！删除
            throw new CustomerException("删除失败，要删除的数据不存在");
        }
        // 当存在，删除
        privilegeMapper.deleteByPrimaryKey(id);
    }
  @Override
    public List<PrivilegeExtend> findAllWithChild() {
        return privilegeExtendMapper.selectAllWithChild();
    }
}