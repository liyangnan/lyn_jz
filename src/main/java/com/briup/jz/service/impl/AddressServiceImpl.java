package com.briup.jz.service.impl;

import com.briup.jz.bean.Address;
import com.briup.jz.bean.AddressExample;
import com.briup.jz.bean.extend.AddressExtend;
import com.briup.jz.dao.AddressMapper;
import com.briup.jz.dao.extend.AddressExtendMapper;
import com.briup.jz.service.IAddressService;
import com.briup.jz.utils.CustomerException;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * @description: 地址实现类
 * @Author: lyn
 * @Date 2020/6/14 11:05
 */
@Service
public class AddressServiceImpl implements IAddressService {

    @Resource
    private AddressMapper addressMapper;
    @Resource
    private AddressExtendMapper addressExtendMapper;
    @Override
    public List<Address> findAll() {
        AddressExample example = new AddressExample();
        return addressMapper.selectByExample(example);
    }

    @Override
    public void saveOrUpdate(Address address) throws CustomerException {
        if(address.getId() != null){
            addressMapper.updateByPrimaryKey(address);
        }else
            addressMapper.insert(address);

    }

    @Override
    public void deleteById(long id) throws CustomerException {
//        先判断该id对应的数据存在与否
        Address address = addressMapper.selectByPrimaryKey(id);
//        数据存在进行删除，数据不存在进行报错
        if(address == null){
//            该id对应的数据不存在
            throw new CustomerException("删除失败，要删除的数据不存在");
        }else
//            数据存在 正常删除
        addressMapper.deleteByPrimaryKey(id);

    }

    @Override
    public List<AddressExtend> findAllWithUser() {
        return addressExtendMapper.selectAllWithUser();
    }
}
