package com.briup.jz.service.impl;

import com.briup.jz.bean.AccountCustomer;
import com.briup.jz.bean.AccountCustomerExample;
import com.briup.jz.bean.extend.AccountCustomerExtend;
import com.briup.jz.dao.AccountCustomerMapper;
import com.briup.jz.dao.extend.AccountCustomerExtendMapper;
import com.briup.jz.service.IAccountCustomerService;
import com.briup.jz.utils.CustomerException;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;


@Service
public class AccountCustomerServiceImpl implements IAccountCustomerService {
    @Resource
    private AccountCustomerMapper accountCustomerMapper;
    @Resource
    private AccountCustomerExtendMapper accountCustomerExtendMapper;

    @Override
    public List<AccountCustomer> findAll() {
        AccountCustomerExample example = new AccountCustomerExample();
        return accountCustomerMapper.selectByExample(example);
    }

    @Override
    public void saveOrUpdate(AccountCustomer accountCustomer) throws CustomerException {
        if(accountCustomer.getId() != null){
            accountCustomerMapper.updateByPrimaryKey(accountCustomer);
        }else {
            accountCustomerMapper.insert(accountCustomer);
        }

    }

    @Override
    public List<AccountCustomer> selectById(long id) throws CustomerException {

        AccountCustomerExample example = new AccountCustomerExample();
        AccountCustomerExample.Criteria  criteria = example.createCriteria();
        criteria.andIdEqualTo(id);
        return accountCustomerMapper.selectByExample(example);
    }

    @Override
    public List<AccountCustomer> selectByuserId(long userId) throws CustomerException {
        AccountCustomerExample example = new AccountCustomerExample();
        AccountCustomerExample.Criteria  criteria = example.createCriteria();
        criteria.andUserIdEqualTo(userId);
        return accountCustomerMapper.selectByExample(example);
    }



    @Override
    public List<AccountCustomerExtend> findAllInfo() {
        return accountCustomerExtendMapper.selectAllInfo();
    }



}
