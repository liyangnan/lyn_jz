package com.briup.jz.service.impl;

import com.briup.jz.bean.Role;
import com.briup.jz.bean.RoleExample;
import com.briup.jz.bean.extend.RoleExtend;
import com.briup.jz.dao.RoleMapper;
import com.briup.jz.dao.extend.RoleExtendMapper;
import com.briup.jz.service.IRoleService;
import com.briup.jz.utils.CustomerException;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;


@Service
public class RoleServiceImpl implements IRoleService {
    @Resource
    private RoleMapper roleMapper;
    @Resource
    private  RoleExtendMapper roleExtendMapper;

    @Override
    public List<Role> findAll() {
        RoleExample example = new RoleExample();
        return roleMapper.selectByExample(example);
    }

    @Override
    public void saveOrUpdate(Role role) throws CustomerException {
        if(role.getId() !=null){
            roleMapper.updateByPrimaryKey(role);
        } else {
            roleMapper.insert(role);
        }
    }

    @Override
    public void deleteById(long id) throws CustomerException {
        // 先判断该id对应的数据存在不？
        Role role = roleMapper.selectByPrimaryKey(id);
        if(role == null){
            //当不存在，报错！删除
            throw new CustomerException("删除失败，要删除的数据不存在");
        }
        // 当存在，删除
       roleMapper.deleteByPrimaryKey(id);
    }

    @Override
    public List<RoleExtend> findAllRole() {
        RoleExample example = new RoleExample();
        return roleExtendMapper.selectAllRole();
    }
}
