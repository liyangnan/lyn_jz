package com.briup.jz.service;

import com.briup.jz.bean.AccountCustomer;
import com.briup.jz.bean.extend.AccountCustomerExtend;
import com.briup.jz.utils.CustomerException;

import java.util.List;

public interface IAccountCustomerService {
    List<AccountCustomer> findAll();

    void saveOrUpdate(AccountCustomer accountCustomer) throws CustomerException;

    List<AccountCustomer> selectById(long id) throws CustomerException;

    List<AccountCustomer> selectByuserId(long userId) throws CustomerException;


    List<AccountCustomerExtend> findAllInfo();



}
