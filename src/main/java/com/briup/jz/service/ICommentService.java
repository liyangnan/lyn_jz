package com.briup.jz.service;

import com.briup.jz.bean.Category;
import com.briup.jz.bean.Comment;
import com.briup.jz.bean.extend.CategoryExtend;
import com.briup.jz.bean.extend.CommentExtend;
import com.briup.jz.utils.CustomerException;

import java.util.List;

public interface ICommentService {
    List<Comment> findAll();
    List<CommentExtend> findAllWithPl();

    void deleteById(long id);

    void saveOrUpdate(Comment comment) ;

    List<CommentExtend> findAllComment();

    Comment findById(long id);
}
