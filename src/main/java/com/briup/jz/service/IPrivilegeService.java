package com.briup.jz.service;

import com.briup.jz.bean.Privilege;
import com.briup.jz.bean.extend.PrivilegeExtend;
import com.briup.jz.utils.CustomerException;

import java.util.List;
public interface IPrivilegeService {
        List<Privilege> findAll();

        void saveOrUpdate(Privilege privilege) throws CustomerException;

        void deleteById(long id) throws CustomerException;

        List<PrivilegeExtend> findAllWithChild();
    }

