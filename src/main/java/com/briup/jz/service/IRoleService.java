package com.briup.jz.service;

import com.briup.jz.bean.Role;
import com.briup.jz.bean.extend.RoleExtend;
import com.briup.jz.utils.CustomerException;

import java.util.List;



public interface IRoleService {
    List<Role> findAll();

    void saveOrUpdate(Role role) throws CustomerException;

    void deleteById(long id) throws CustomerException;

    List<RoleExtend> findAllRole();
}
