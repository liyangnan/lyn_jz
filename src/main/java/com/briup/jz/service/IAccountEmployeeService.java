package com.briup.jz.service;

import com.briup.jz.bean.AccountEmployee;
import com.briup.jz.utils.CustomerException;


import java.util.List;

public interface IAccountEmployeeService {
    List<AccountEmployee> findAll();

    void saveOrUpdate(AccountEmployee accountEmployee) ;


    List<AccountEmployee> selectById(long id) throws CustomerException;

    List<AccountEmployee> selectByuserId(long userId) throws CustomerException;
}

