package com.briup.jz.service;

import com.briup.jz.bean.Category;
import com.briup.jz.bean.extend.CategoryExtend;
import com.briup.jz.utils.CustomerException;

import java.util.List;

public interface ICategoryService {

    List<Category> findAll();

    void saveOrUpdate(Category category) throws CustomerException;

    void deleteById(long id) throws CustomerException;

    List<CategoryExtend> findAllWithChild();
}
