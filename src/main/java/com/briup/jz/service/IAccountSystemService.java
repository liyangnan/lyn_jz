package com.briup.jz.service;


import com.briup.jz.bean.AccountSystem;
import com.briup.jz.utils.CustomerException;


import java.util.List;

public interface IAccountSystemService {
    List<AccountSystem> findAll();

    void saveOrUpdate(AccountSystem accountSystem) ;


    List<AccountSystem> selectById(long id) throws CustomerException;

    List<AccountSystem> selectByuserId(long userId) throws CustomerException;

}
