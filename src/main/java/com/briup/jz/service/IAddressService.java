package com.briup.jz.service;

import com.briup.jz.bean.Address;
import com.briup.jz.bean.extend.AddressExtend;
import com.briup.jz.utils.CustomerException;

import java.util.List;

/**
 * @description:
 * @Author: lyn
 * @Date 2020/6/14 11:03
 */
public interface IAddressService {

    List<Address> findAll();

    void saveOrUpdate(Address address) throws CustomerException;

    void deleteById(long id) throws CustomerException;

    List<AddressExtend> findAllWithUser();
}
