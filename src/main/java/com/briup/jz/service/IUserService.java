package com.briup.jz.service;

import com.briup.jz.bean.User;
import com.briup.jz.bean.extend.UserExtend;
import com.briup.jz.utils.CustomerException;

import java.util.List;

public interface IUserService {

    List<User> findAll();

    List<UserExtend> findAllEmployee();

    List<UserExtend> findAllCustomer();

    void saveOrUpdate(User user) throws CustomerException;

    void deleteById(long id) throws CustomerException;

    List<UserExtend> findAllWithRole();

    User findById(long id);

}
