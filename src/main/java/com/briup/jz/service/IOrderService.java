package com.briup.jz.service;

import com.briup.jz.bean.Order;
import com.briup.jz.bean.extend.OrderExtend;
import com.briup.jz.utils.CustomerException;
import com.briup.jz.vm.OrderVM;
import com.sun.org.apache.xpath.internal.operations.Or;

import java.util.List;

public interface IOrderService {

    void commit(OrderVM orderVM) throws CustomerException;

    List<Order> findeAll();

    List<OrderExtend> query(String status);

//    通过id具体查询订单
    OrderExtend findOrderDetalsById(long id);

//    支付后订单进入待派单状态
    void payOrder (long orderId) throws CustomerException;

//    派单后订单进入待服务状态
    void sendOrder(long orderId, long employeeId) throws CustomerException;

//    接单后订单进入待服务状态
//    void takeOrder(long orderId) throws Exception;

//    服务后订单进入待确认状态
    void rejectOrder(long orderId) throws CustomerException;

//    确认后订单进入已完成状态
    void confirmOrder(long orderId) throws CustomerException;
}
