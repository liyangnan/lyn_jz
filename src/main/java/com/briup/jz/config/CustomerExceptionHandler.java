package com.briup.jz.config;

import com.briup.jz.utils.CustomerException;
import com.briup.jz.utils.Message;
import com.briup.jz.utils.MessageUtil;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

//统一异常类，对控制器层跑出来的一场进行统一处理



@RestControllerAdvice
public class CustomerExceptionHandler {

    @ExceptionHandler(value =  Exception.class) // 捕获 Controller 中抛出的指定类型的异常，也可以指定其他异常
    public <E> Message handler(Exception exception){
//        输出异常信息
        exception.printStackTrace();
//        如果是自定义异常，抛出错误信息
        if(exception instanceof CustomerException){

            return MessageUtil.error(exception.getMessage());
        } else if (exception instanceof DataIntegrityViolationException) {
//            如果是数据库异常，一般出现在外键关联的时候
        	return MessageUtil.error("该数据暂时不允许删除！请先删除与当前数据关联的其他数据");
        }
        return MessageUtil.error("后台接口异常！");
    }
}
