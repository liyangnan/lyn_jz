package com.briup.jz.bean.extend;

import com.briup.jz.bean.Address;
import com.briup.jz.bean.Order;
import com.briup.jz.bean.OrderLine;
import com.briup.jz.bean.User;

import java.util.List;

/**
 * @description: 订单拓展类
 * @Author: lyn
 * @Date 2020/6/13 22:13
 */
public class OrderExtend extends Order {
    public static final String STATUS_WFK = "未付款";
    public static final String STATUS_DPD = "待派单";
    public static final String STATUS_DFW = "待服务";
    public static final String STATUS_DQR = "待确认";
    public static final String STATUS_YWC = "已完成";

    private User customer;
    private User employee;
    private Address address;
    private List<OrderLine> orderLines;

    public User getEmployee() {
        return employee;
    }

    public void setEmployee(User employee) {
        this.employee = employee;
    }

    public List<OrderLine> getOrderLines() {
        return orderLines;
    }

    public void setOrderLines(List<OrderLine> orderLines) {
        this.orderLines = orderLines;
    }

    public User getCustomer() {
        return customer;
    }

    public void setCustomer(User customer) {
        this.customer = customer;
    }
    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }
}