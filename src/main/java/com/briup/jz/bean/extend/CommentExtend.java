package com.briup.jz.bean.extend;

import com.briup.jz.bean.Category;
import com.briup.jz.bean.Comment;
import com.briup.jz.bean.Order;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public class CommentExtend extends Comment {
    private List<Comment> pl;

    public List<Comment> getPl() {
        return pl;
    }

    public void setPl(List<Comment> Pl) {
        this.pl = pl;
    }
}
