package com.briup.jz.bean.extend;

import com.briup.jz.bean.Address;
import com.briup.jz.bean.User;

/**
 * @description: 地址拓展类
 * @Author: lyn
 * @Date 2020/6/14 11:31
 */
public class AddressExtend extends Address {
    private User user;

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
