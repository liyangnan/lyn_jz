package com.briup.jz.bean.extend;

import com.briup.jz.bean.OrderLine;
import com.briup.jz.bean.Product;

/**
 * @description: 订单项的拓展类
 * @Author: lyn
 * @Date 2020/6/14 9:44
 */
public class OrderLineExtend extends OrderLine {

    private Product product;

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }
}
