package com.briup.jz.bean.extend;

import com.briup.jz.bean.AccountEmployee;
import com.briup.jz.bean.Order;
import com.briup.jz.bean.Role;
import com.briup.jz.bean.User;

public class AccountEmployeeExtend extends AccountEmployee {
    private User user;
    private Order order;

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Order getOrder() {
        return order;
    }

    public void setOrder(Order order) {
        this.order = order;
    }
 }


