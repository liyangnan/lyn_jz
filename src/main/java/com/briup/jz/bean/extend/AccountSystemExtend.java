package com.briup.jz.bean.extend;

import com.briup.jz.bean.AccountSystem;
import com.briup.jz.bean.Order;
import com.briup.jz.bean.User;

public class AccountSystemExtend extends AccountSystem{
    private User user;
    private Order order;

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Order getOrder() {
        return order;
    }

    public void setOrder(Order order) {
        this.order = order;
    }
}

