package com.briup.jz.bean.extend;
import com.briup.jz.bean.Privilege;

import java.util.List;
public class PrivilegeExtend extends Privilege {
    private List<Privilege> children;

    public List<Privilege> getChildren() {
        return children;
    }

    public void setChildren(List<Privilege> children) {
        this.children = children;
    }
}
