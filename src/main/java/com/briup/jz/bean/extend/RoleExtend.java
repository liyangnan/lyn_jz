package com.briup.jz.bean.extend;

import com.briup.jz.bean.Role;


public class RoleExtend extends Role {
    private Role role;

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }
}
