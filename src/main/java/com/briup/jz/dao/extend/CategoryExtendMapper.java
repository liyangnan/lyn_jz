package com.briup.jz.dao.extend;

import com.briup.jz.bean.Category;
import com.briup.jz.bean.extend.CategoryExtend;

import java.util.List;

public interface CategoryExtendMapper {

    List<CategoryExtend> selectAllWithChild();
    List<Category> selectByParentId();

}
