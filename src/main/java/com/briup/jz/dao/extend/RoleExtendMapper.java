package com.briup.jz.dao.extend;


import com.briup.jz.bean.extend.RoleExtend;

import java.util.List;

public interface RoleExtendMapper {
    List<RoleExtend> selectAllRole();
}
