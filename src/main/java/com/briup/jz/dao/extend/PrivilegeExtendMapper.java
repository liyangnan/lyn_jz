package com.briup.jz.dao.extend;
import com.briup.jz.bean.extend.PrivilegeExtend;

import java.util.List;

public interface PrivilegeExtendMapper {

        List<PrivilegeExtend> selectAllWithChild();
    }

