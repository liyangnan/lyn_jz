package com.briup.jz.dao.extend;

import com.briup.jz.bean.extend.AccountEmployeeExtend;

import java.util.List;

public interface AccountEmployeeExtendMapper {
    List<AccountEmployeeExtend> selectAccountEmployee();
    List<AccountEmployeeExtend> selectAllInfo();
    List<AccountEmployeeExtend> selectByUserId(long userId);
    List<AccountEmployeeExtend> selectById(long Id);
}
