package com.briup.jz.dao.extend;

import com.briup.jz.bean.extend.OrderExtend;

import java.util.List;

/**
 * @description: 订单接口
 * @Author: lyn
 * @Date 2020/6/14 8:59
 */
public interface OrderExtendMapper {

//    多条件符合查询

    List<OrderExtend> query(Long id,String status);

}
