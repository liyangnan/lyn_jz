package com.briup.jz.dao.extend;

import com.briup.jz.bean.extend.RolePrivilegeExtend;

import java.util.List;



public interface RolePrivilegeExtendMapper {

    List<RolePrivilegeExtend> selectAllWithRole();

    List<RolePrivilegeExtend> selectAllWithPrivilege();
}
