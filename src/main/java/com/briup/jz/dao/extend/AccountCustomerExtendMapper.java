package com.briup.jz.dao.extend;

import com.briup.jz.bean.extend.AccountCustomerExtend;

import java.util.List;

public interface AccountCustomerExtendMapper {

   // List<AccountCustomerExtend> selectAllWithAccountCustomer();

    List<AccountCustomerExtend> selectAllInfo();
    List<AccountCustomerExtend> selectByUserId(long userId);
    List<AccountCustomerExtend> selectById(long Id);
}
