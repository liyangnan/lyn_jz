package com.briup.jz.dao.extend;

import com.briup.jz.bean.extend.AddressExtend;

import java.util.List;

/**
 * @description: 地址接口
 * @Author: lyn
 * @Date 2020/6/14 11:39
 */
public interface AddressExtendMapper {

    List<AddressExtend> selectAllWithUser();
}
