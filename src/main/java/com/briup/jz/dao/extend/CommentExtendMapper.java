package com.briup.jz.dao.extend;

import com.briup.jz.bean.Comment;
import com.briup.jz.bean.extend.CategoryExtend;
import com.briup.jz.bean.extend.CommentExtend;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface CommentExtendMapper {
    List<CommentExtend> selectAllWithPl();
    List<Comment> selectById();
    List<CommentExtend> selectAllComment();

}
