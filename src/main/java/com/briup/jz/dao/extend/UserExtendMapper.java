package com.briup.jz.dao.extend;

import com.briup.jz.bean.extend.UserExtend;

import java.util.List;

public interface UserExtendMapper {

    List<UserExtend> selectAllWithRole();

    List<UserExtend> selectAllEmployee();

    List<UserExtend> selectAllCustomer();
}
