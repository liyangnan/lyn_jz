package com.briup.jz.dao.extend;

import com.briup.jz.bean.extend.AccountSystemExtend;

import java.util.List;

public interface AccountSystemExtendMapper {
    List<AccountSystemExtend> selectaccountsystem();

    List<AccountSystemExtend> selectAllInfo();

    List<AccountSystemExtend>selectByUserId(long userId);


    List<AccountSystemExtend> selectById(long Id);
}
