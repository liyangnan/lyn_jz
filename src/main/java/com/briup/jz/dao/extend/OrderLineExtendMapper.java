package com.briup.jz.dao.extend;

import com.briup.jz.bean.OrderLine;

import java.util.List;

/**
 * @description: 订单项接口
 * @Author: lyn
 * @Date 2020/6/14 9:57
 */
public interface OrderLineExtendMapper {

    List<OrderLine> selectOrderDetalsById(long id);
}
